package evemap

import (
	"net/url"
	"net/http"
	"fmt"
	"log"
	"time"
	"encoding/json"
)



func GetMapFormat(region string) (format map[string]interface{}){

	safeRegion := url.QueryEscape(region)


	urlS3 := fmt.Sprintf("https://s3-ap-southeast-2.amazonaws.com/spyglass-ng/%s_format.json", safeRegion)

	req, err := http.NewRequest("GET", urlS3, nil)

	if err != nil {
		log.Fatal("NewRequest: ", err)
		return
	}

	client := &http.Client{Timeout:time.Second*30}

	resp, err := client.Do(req)

	if err != nil {
		log.Fatal("Do: ", err)
		return
	}

	defer resp.Body.Close()


	var f interface{}

	if err := json.NewDecoder(resp.Body).Decode(&f); err != nil {
		log.Println(err)
	}

	if f == nil {
		return nil
	}

	temp := f.(map[string]interface{})

	return temp
}

func GetMapList() (map[string]interface{}){

	urlS3 := "https://s3-ap-southeast-2.amazonaws.com/spyglass-ng/region_list.json"

	req, err := http.NewRequest("GET", urlS3, nil)

	if err != nil {
		log.Fatal("NewRequest: ", err)
	}

	client := &http.Client{Timeout:time.Second*30}

	resp, err := client.Do(req)

	defer resp.Body.Close()

	if err != nil {
		log.Fatal("Do: ", err)
		return nil
	}

	var f  interface{}

	if err := json.NewDecoder(resp.Body).Decode(&f); err != nil {
		log.Println(err)
	}

	temp := f.(map[string]interface{})

	return temp

}