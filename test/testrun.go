package main

import (
	"github.com/pkg/profile"
	"gitlab.com/spyglass/evemap"
)

func main() {

	defer profile.Start(profile.CPUProfile).Stop()



	evemap.MapServer()


}


