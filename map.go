package evemap

import (
	"strconv"
	"github.com/ajstarks/svgo"
	"fmt"
				"net/http"
				)

type Map struct {
	Name      string
	Systems   []System
	Jumps     []Stargate
	MapFactor float64
}

type System struct {
	Name string
	ID   float64

	CenterX float64
	CenterY float64

	Neighbours []Neighbour
}

type Neighbour struct {
	ID   float64
	Name string
}

type Stargate struct {
	S1 Neighbour
	S2 Neighbour

	x1 float64
	y1 float64

	x2 float64
	y2 float64

	//Reserved for future use
	Class string
}

func NewMap(region string) (mip *Map) {

	mapFormat := GetMapFormat(region)

	var systems []System

	for _, v := range mapFormat {

		v1 := v.(map[string]interface{})

		name := v1["name"]
		id := v1["id"]

		nb := v1["neighbours"]
		nb1 := nb.(map[string]interface{})

		var nbl []Neighbour

		for nbi, nbn := range nb1 {

			nbid, _ := strconv.ParseFloat(nbi, 64)

			n := Neighbour{ID: nbid, Name: nbn.(string)}

			nbl = append(nbl, n)
		}

		coords := v1["coords"].(map[string]interface{})
		cx := coords["center_x"]
		cy := coords["center_y"]

		sys := System{Name: name.(string), ID: id.(float64), CenterX: cx.(float64), CenterY: cy.(float64), Neighbours: nbl}

		systems = append(systems, sys)
	}

	mappy := Map{Name: region, Systems: systems, MapFactor: 1.3}

	return &mappy

}

func (mip *Map) BuildJumps() () {

	mip.Jumps = nil

	var newjumps []Stargate

	for _, system := range mip.Systems {

		nbs := system.Neighbours

		for _, system2 := range nbs {

			s1 := Neighbour{ID: system.ID, Name: system.Name}

			s2 := mip.FindSystemByID(system2.ID)

			sg := Stargate{S1: s1, S2: system2, x1: system.CenterX, y1: system.CenterY, x2: s2.CenterX, y2: s2.CenterY}

			match := false

			for _, sgt := range newjumps {
				if stargateEquality(sgt, sg) {
					match = true
					break
				}
			}

			if !match {
				newjumps = append(newjumps, sg)
			}
		}
	}

	mip.Jumps = newjumps
}

func (mp *Map) FindSystemByID(id float64) (sys System) {
	for _, system := range mp.Systems {
		if system.ID == id {
			return system
		}
	}

	return
}

func stargateEquality(s1 Stargate, s2 Stargate) (equal bool) {

	if s1.S1.ID == s2.S1.ID && s1.S2.ID == s2.S2.ID {
		return true
	} else if s1.S2.ID == s2.S1.ID && s1.S1.ID == s2.S2.ID {
		return true
	} else {
		return false
	}
}

func (mip *Map) BuildMapSVG(w http.ResponseWriter, r *http.Request) () {

	w.Header().Set("Content-Type", "image/svg+xml")

	svgCanvas := svg.New(w)

	//svgCanvas.Start(int(1229*mip.MapFactor), int(922*mip.MapFactor))
	fmt.Fprintln(svgCanvas.Writer, "<svg width=\"100%\" height=\"100%\" xmlns=\"http://www.w3.org/2000/svg\" xmlns:xlink=\"http://www.w3.org/1999/xlink\" id=\"evemap\">") //, int(1229*mip.MapFactor),int(922*mip.MapFactor))

	svgCanvas.Title(mip.Name)

	for _, jump := range mip.Jumps {
		mip.svgDrawJump(svgCanvas, jump)
	}

	for _, sys := range mip.Systems {
		mip.svgDrawSystem(svgCanvas, sys)
	}

	svgCanvas.End()

}

func (mip *Map) svgDrawSystem(s *svg.SVG, system System) {

	s.Gtransform(fmt.Sprintf("translate(%d,%d)", int(system.CenterX*mip.MapFactor)-25, int(system.CenterY*mip.MapFactor)-16))

	stat_bg := "M0 22 h 50 v 6 q 0 5, -5 5 h -40 q -5 0, -5 -5 Z"
	alarm_bg := "M5 0 h 40 q 5 0, 5 5 v 17 h -50 v -17 q 0 -5, 5 -5 Z"

	s.Gid(fmt.Sprintf("%s_stat", system.Name))
	s.Path(stat_bg, "fill=\"#0000ff\"")
	s.Gend()

	s.Gid(fmt.Sprintf("%s_alm", system.Name))
	s.Path(alarm_bg, "fill=\"#ffffff\"")
	s.Gend()

	s.Gid(fmt.Sprintf("%s_alm_str", system.Name))
	s.Text(25, 20, "status", " text-anchor=\"middle\" font-family=\"Helvetica, Arial, sans-serif\" font-size=\"8\"")
	s.Gend()

	s.Text(25, 9, system.Name, " text-anchor=\"middle\" font-family=\"Helvetica, Arial, sans-serif\" font-size=\"9\"")

	s.Roundrect(0, 0, 50, 33, 5, 5, "fill=\"none\" stroke=\"#000\"")

	s.Gend()
}

func (mip *Map) svgDrawJump(s *svg.SVG, sg Stargate) {
	s.Line(int(sg.x1*mip.MapFactor), int(sg.y1*mip.MapFactor), int(sg.x2*mip.MapFactor), int(sg.y2*mip.MapFactor), "stroke=\"black\"")
}

func (mip *Map) GetSystemList() (list []string) {

	for _, sys := range mip.Systems {
		list = append(list, sys.Name)
	}

	return
}
