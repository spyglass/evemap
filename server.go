package evemap

import (
	"encoding/json"
	"fmt"
	"net/http"
	"strings"
	"strconv"
	"github.com/rs/cors"
)

var (
	addr = ":13272"
	IDMap = IDMapping{}
	Uni NewEden
)

func MapServer() {

	IDMap = NewIDMap()
	Uni = CreateUniverse()

	mux := http.NewServeMux()

	mux.HandleFunc("/region/systems/", serveJSONMap)
	mux.HandleFunc("/region/", serveRegionMap)
	mux.HandleFunc("/version", serveVersion)
	mux.HandleFunc("/distance/", serveDistance)
	mux.HandleFunc("/regions", serveRegionList)

	handler := cors.Default().Handler(mux)

	if err := http.ListenAndServe(addr, handler); err != nil {
		panic(err)
	}
}


func serveRegionMap(w http.ResponseWriter, r *http.Request) () {

	region := strings.TrimPrefix(r.URL.Path, "/region/")
	if !isValidRegion(region){
		w.WriteHeader(418) // I am a teapot
		return
	}
	w.Header().Set("Content-Type", "image/svg+xml")
	w.WriteHeader(http.StatusOK)

	mip := NewMap(region)
	mip.BuildJumps()
	mip.BuildMapSVG(w, r)

}

func serveVersion(w http.ResponseWriter, r *http.Request) () {
	w.WriteHeader(http.StatusOK)
	fmt.Fprint(w, version)
}

func serveRegionList(w http.ResponseWriter, r *http.Request) {
	w.WriteHeader(http.StatusOK)
	w.Header().Set("Content-Type", "application/json")
	json.NewEncoder(w).Encode(Uni.mapList)
}

func serveJSONMap(w http.ResponseWriter, r *http.Request) () {

	region := strings.TrimPrefix(r.URL.Path, "/region/systems/")
	if !isValidRegion(region){
		w.WriteHeader(418) // I am a teapot
		return
	}


	w.Header().Set("Content-Type", "application/json")
	w.WriteHeader(http.StatusOK)

	mip := NewMap(region)
	mip.BuildJumps()
	json.NewEncoder(w).Encode(mip)
}

func isValidRegion(region string) (result bool) {

	for item, _ := range GetMapList() {
		if region == item {
			return true
		}
	}

	return false

}

func serveDistance(w http.ResponseWriter, r *http.Request){
	path := strings.TrimPrefix(r.URL.Path, "/distance/")
	split := strings.Split(path, "/")

	if len(split) != 2 {
		w.WriteHeader(http.StatusBadRequest)
		fmt.Fprint(w, "Invalid request, should be of the form '/distance/StartSystem/DestinationSystem")
		return
	}

	if strings.HasSuffix(split[1], "/") {
		split[1] = strings.TrimSuffix(split[1], "/")
	}

	if !isValidSystemName(split[0]){
		w.WriteHeader(http.StatusBadRequest)
		fmt.Fprint(w, "Invalid source system")
		return
	}

	if !isValidSystemName(split[1]){
		w.WriteHeader(http.StatusBadRequest)
		fmt.Fprint(w, "Invalid destination system")
		return
	}

	// So we have two valid system names, now get the distance between them
	// First translate to system ids then get and return the sep distance

	source, _ := strconv.Atoi(IDMap.NameToID[split[0]].(string))
	dest, _ := strconv.Atoi(IDMap.NameToID[split[1]].(string))

	dst := Uni.FindShortestRoute(uint(source),uint(dest))

	if dst < 0 { //Either Unreachable or we have done fucked up... Either way it broke
		w.WriteHeader(http.StatusInternalServerError)
		fmt.Fprint(w, "Could not calculate number of jumps between systems")
		return
	}

	// We got here so we have a distance and its good to go. Format and return.

	w.WriteHeader(http.StatusOK)
	fmt.Fprintf(w, "{\n\t\"distance\": %d\n}", dst)
	return
}

func isValidSystemName(name string) (valid bool) {
	if IDMap.NameToID[name] != nil {
		return true
	} else {
		return false
	}
}