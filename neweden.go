package evemap

import (
	"net/http"
	"time"
	"log"
	"bufio"
	"strings"
	"strconv"
	"fmt"
	"math"
	"io/ioutil"
	"encoding/json"
)

const (
	maxSysID      = 30045354
	minSysID      = 30000001
	systemCount   = maxSysID - minSysID
	bitsPerStruct = 32
)

type NewEden struct {
	adjacencyMatrix [][]uint32

	systems []Sys

	mapList *map[string]string
}

type Sys struct {
	id      uint
	name    string
	truesec float32

	// The following are for the BFS
	visited bool
	parent  uint
}

func blankSlate() (uni NewEden) {

	// We are adressing individual bits, lets create an array of the appropriate size

	numSys := systemCount + 1
	numInts := int(math.Ceil(float64(numSys) / float64(bitsPerStruct)))

	syList := make([]Sys, numSys)

	for a := range syList {
		syList[a] = Sys{}
	}

	adMatrix := make([][]uint32, numSys)
	for i := range adMatrix {
		adMatrix[i] = make([]uint32, numInts)

		for j := range adMatrix[i] {
			adMatrix[i][j] = 0
		}
	}

	uni = NewEden{adjacencyMatrix: adMatrix, systems: syList}

	return uni
}

func IdToArrayPos(id uint) (arrayPos uint, intPos uint) {
	relative := IdToRelativeID(id)

	arrayPos = uint(relative / bitsPerStruct)
	intPos = uint(relative % bitsPerStruct)

	return arrayPos, intPos
}

func IdToRelativeID(id uint) (relId uint) {

	return id - minSysID
}

func RelativeIDToID(relID uint) (id uint) {
	return relID + minSysID
}

func ArrayPosToID(arrayPos uint, intPos uint) (id uint) {

	arrayPos = arrayPos * bitsPerStruct
	id = RelativeIDToID(arrayPos + intPos)

	return id
}

func CreateUniverse() (universe NewEden) {

	idURL := "https://s3-ap-southeast-2.amazonaws.com/spyglass-ng/neweden.dot"
	client := http.Client{Timeout: time.Second * 15} // Max fifteen seconds
	req, err := http.NewRequest(http.MethodGet, idURL, nil)
	if err != nil {
		log.Fatal(err)
	}

	req.Header.Set("User-Agent", "spyglass-ng")

	res, getErr := client.Do(req)
	if getErr != nil {
		log.Fatal(err)
	}

	universe = blankSlate()

	// Get a temporary ID mapping to use
	idsToNames := NewIDMap()

	scanner := bufio.NewScanner(res.Body)

	for scanner.Scan() {
		line := scanner.Text()
		line = strings.TrimSpace(line)

		// Try to find an edge definiton

		split := strings.Split(line, " -- ")

		if len(split) != 2 {
			continue
		}

		//Most likely a edge definition, last check is if end of string 2 is a semi-colon

		if !strings.HasSuffix(split[1], ";") {
			continue
		}

		//Woohoo an edge definition, get the node ids, make them and add them to the adjacency matrix

		sys1, err1 := strconv.Atoi(split[0])
		sys2, err2 := strconv.Atoi(strings.TrimSuffix(split[1], ";"))

		if err1 != nil || err2 != nil {

			//error parsing the system ids... Just skip them
			log.Println("Error parsing system ids from " + split[0] + " and " + split[1])
			continue
		}

		sys1ID, _ := idsToNames.IDToName[strconv.Itoa(sys1)]
		sys2ID, _ := idsToNames.IDToName[strconv.Itoa(sys2)]

		universe.addSystem(uint(sys1), sys1ID.(string), 0.0)
		universe.addSystem(uint(sys2), sys2ID.(string), 0.0)
		universe.addEdge(uint(sys1), uint(sys2))

	}

	regionListUrl := "https://s3-ap-southeast-2.amazonaws.com/spyglass-ng/region_list.json"

	req, err = http.NewRequest(http.MethodGet, regionListUrl, nil)
	if err != nil {
		log.Fatal(err)
	}

	res, err = client.Do(req)
	if err != nil{
		log.Fatal(err)
	}

	body, err := ioutil.ReadAll(res.Body)
	if err != nil {
		log.Fatal(err)
	}

	maplist := make(map[string]string)
	universe.mapList = &maplist

	jsonErr := json.Unmarshal(body ,universe.mapList)
	if jsonErr != nil {
		panic(jsonErr)
	}

	return universe
}

//addSystem will add a system to a graph, overwrites existing
func (uni *NewEden) addSystem(sysID uint, name string, truesec float32) () {
	sys := Sys{id: sysID, name: name, truesec: truesec}
	uni.systems[IdToRelativeID(sysID)] = sys
}

func (uni *NewEden) addEdge(source uint, dest uint) () {

	arrayPos1, intPos1 := IdToArrayPos(source)
	arrayPos2, intPos2 := IdToArrayPos(dest)

	relID1 := IdToRelativeID(source)
	relID2 := IdToRelativeID(dest)

	sourceAdj := uni.adjacencyMatrix[relID1]
	destAdj := uni.adjacencyMatrix[relID2]

	// Put a 1 in the source systems adjacency list at the position for the dest system
	destInt := sourceAdj[arrayPos2]
	destIntPos := uint32(1) << intPos2
	newDestInt := destInt | destIntPos
	uni.adjacencyMatrix[relID1][arrayPos2] = newDestInt

	// Put a 1 in the source systems adjacency list at the position for the dest system
	sourceInt := destAdj[arrayPos1]
	sourceIntPos := uint32(1) << intPos1
	newSourceInt := sourceInt | sourceIntPos
	uni.adjacencyMatrix[relID2][arrayPos1] = newSourceInt

}

func (uni *NewEden) GetSystems() (systems []Sys) {
	return uni.systems
}

func (uni *NewEden) GetSystem(id uint) (sys *Sys) {
	if id >= minSysID {
		id = IdToRelativeID(id)
	}
	return &uni.systems[id]
}

func (uni *NewEden) EdgeExists(source uint, dest uint) (exists bool) {
	exists = false

	arrayPos1, intPos1 := IdToArrayPos(source)
	arrayPos2, intPos2 := IdToArrayPos(dest)

	sourceRel := IdToRelativeID(source)
	destRel := IdToRelativeID(dest)

	var int1Pos uint32
	var int2Pos uint32

	int1Pos = uint32(uint(1) << intPos1)
	int2Pos = uint32(uint(1) << intPos2)

	sourceAdj := uni.adjacencyMatrix[sourceRel]
	destAdj := uni.adjacencyMatrix[destRel]

	srcToDest := (sourceAdj[arrayPos2] & int2Pos) > 0
	destToSrc := (destAdj[arrayPos1] & int1Pos) > 0

	exists = srcToDest || destToSrc

	return exists
}

func (uni *NewEden) GetEdges(source uint) (neighbours []uint) {

	relID := source

	if source >= minSysID {
		relID = IdToRelativeID(source)
	}

	sourceAdj := uni.adjacencyMatrix[relID]

	for arrayPos := range sourceAdj {

		arrayInt := sourceAdj[arrayPos]

		for bitPos := uint32(0); bitPos < bitsPerStruct; bitPos++ {

			bit := uint32(uint(1) << bitPos)

			edge := arrayInt & bit

			if edge > 0 {
				neighbour := ArrayPosToID(uint(arrayPos), uint(bitPos))
				neighbours = append(neighbours, neighbour)
			}
		}
	}

	return neighbours
}

func (uni *NewEden) FindShortestRoute(source uint, dest uint) (dist int) {

	//fmt.Printf("\t src, dest -> %v, %v\n", source, dest)

	dist = -1

	// First confirm both systems exist
	if uni.systems[IdToRelativeID(source)].id != source {
		fmt.Println("Invalid Source")
		return
	}

	if uni.systems[IdToRelativeID(dest)].id != dest {
		fmt.Println("Invalid Destination")
		return
	}

	// Both Systems are valid. If they are the same it is 0 jumps...

	dist = 0
	if source == dest {
		fmt.Println("Navigating to the same system")
		return
	}

	// Nope we have to travel.

	//Reset the graph
	for sy := range uni.systems {
		uni.systems[sy].visited = false
		uni.systems[sy].parent = 0
	}

	// Set the root to visited with a depth of zero and begin BFS

	uni.systems[IdToRelativeID(source)].visited = true
	uni.systems[IdToRelativeID(source)].parent = 0

	qu := NewQueue()
	qu.Push(uint(source))

	for qu.Len() > 0 {

		//We are going a jump, add 1 to the distance
		idToCheck := qu.Poll().(uint)
		relID := idToCheck

		if relID > minSysID {
			relID = IdToRelativeID(idToCheck)
		}

		if uni.GetSystem(relID).id == dest {
			break
		}

		adjacent := uni.GetEdges(idToCheck)

		for n := range adjacent {

			adj := adjacent[n]

			if uni.GetSystem(adj).visited == true {
				continue
			}

			uni.GetSystem(adj).visited = true
			uni.GetSystem(adj).parent = idToCheck

			qu.Push(uint(adj))

		}

	}

	// If we are out of the loop one of two things has happpened,
	// We have reached the destination and can now trace our way back,
	// Or there is no path and we ran out of systems...

	destination := uni.GetSystem(dest)

	dist = -1

	if destination.visited == false {
		// We never made a path
		return
	}

	// We have a path so now just follow the parents back to the source
	qu = NewQueue()

	qu.Push(destination.id)

	for qu.Len() > 0 {

		dist++

		idToCheck := qu.Poll().(uint)

		if idToCheck == source {
			break
		}

		qu.Push(uni.GetSystem(idToCheck).parent)
	}

	return dist
}

func (uni *NewEden) RadiantBenchmark() {

	iID := uni.systems[0].id
	fmt.Println(strconv.Itoa(int(iID)))
	for j := range uni.GetSystems() {
		jID := uni.systems[j].id

		if iID < minSysID || jID < minSysID {
			continue
		}
		uni.FindShortestRoute(iID, jID)

	}

}
