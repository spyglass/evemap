package evemap

import (
	"encoding/json"
	"log"
	"net/http"
	"time"
	"io/ioutil"
)

type IDMapping struct {
	IDToName map[string]interface{}
	NameToID map[string]interface{}
}


func NewIDMap() (IDMapping) {

	mip := IDMapping{}

	idURL := "https://s3-ap-southeast-2.amazonaws.com/spyglass-ng/IDToName.json"
	client := http.Client{Timeout:time.Second * 5} // Max two seconds
	req, err := http.NewRequest(http.MethodGet, idURL, nil)
	if err != nil {
		log.Fatal(err)
	}

	req.Header.Set("User-Agent", "spyglass-ng")

	res, getErr := client.Do(req)
	if getErr != nil {
		log.Fatal(err)
	}

	body, readErr := ioutil.ReadAll(res.Body)
	if readErr != nil {
		log.Fatal(err)
	}


	jsonErr := json.Unmarshal(body, &mip.IDToName)
	if jsonErr != nil {
		log.Fatal(err)
	}

	nameURL := "https://s3-ap-southeast-2.amazonaws.com/spyglass-ng/NameToID.json"
	req, err = http.NewRequest(http.MethodGet, nameURL, nil)
	if err != nil {
		log.Fatal(err)
	}

	req.Header.Set("User-Agent", "spyglass-ng")

	res, getErr = client.Do(req)
	if getErr != nil {
		log.Fatal(err)
	}

	body, readErr = ioutil.ReadAll(res.Body)
	if readErr != nil {
		log.Fatal(err)
	}

	jsonErr = json.Unmarshal(body, &mip.NameToID)
	if jsonErr != nil {
		log.Fatal(err)
	}


	return mip
}

func (id IDMapping) NOP () () {

}

